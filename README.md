# py_intro

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/the0den%2Fpy_intro/master?filepath=index.ipynb)

Python introduction Jupyter Notebooks.

The easiest way to launch - using the Binder badge above.